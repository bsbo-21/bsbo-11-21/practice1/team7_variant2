```plantuml
@startuml
skin rose
title Модель предметной области

class SearchEngine {
    - Экземпляр класса: LCard_List
    - Экземпляр класса: Books_List
    - ID клиента: int
    + Запрос каталога(): void
    + Запрос читательского билета(int): LibraryCard
    + Запрос книги(string): Book
    + Возврат книги(Book): void
}

class LCard_List {
    - Читательские билеты: Dictionary<int, LibraryCard>
    + Выдача читательского билета(int): LibraryCard
    + Добавление книги в список долгов(int, Book): void
    + Удаление книги из списка долгов(int, Book): void
}

class Books_List {
    - Книги: Dictionary<string, Book>
    + Выдача всех книг(): Book[]
    + Выдача книги(string): Book
    + Добавление книги(Book): void
}

class BooksCatalog {
    - Книги: Book[]
    + Вывод каталога клиенту(): void
}

class LibraryCard {
    - ID: int
    - Имя: string
    - Работа/Учёба: string
    - Долги: Dictionary<Book, DateTime>
}

class Book {
    - ID: int
    - Название: string
    - Автор: string 
    - Издательство: string
    - Год издания: int
    - Количество страниц: int
    - Статус 'взята'/'в наличии': bool
}

SearchEngine "1" -- "1" LCard_List
SearchEngine "1" -- "1" Books_List
LCard_List "1" -- "1..many" LibraryCard
Books_List "1" -- "1..many" Book
BooksCatalog "0..many" -- "1" Books_List
LibraryCard "1" -- "0..many" Book

@enduml
```

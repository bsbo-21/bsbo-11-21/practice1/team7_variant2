# Диаграмма прецедентов UML для библиотечной системы

```plantuml 
@startuml
left to right direction
actor Клиент as cl
actor Сотрудник as em
actor Поисковая_Система as se
rectangle ИС_Библиотеки {
  (Добавление\nкниги в фонд) as add
  (Списание\nкниги из\nфонда) as del
  (Регистрация) as reg
  (Взять книгу) as take
  (Вернуть книгу) as back
  (Информация\nпо запросу) as info
  (Выбор книги\nиз каталога) as choice
}
add -- em
del -- em
cl -- reg
cl -- take
cl -- back
cl -- info
cl -- choice
reg -- em
take -- em
back -- em
info -- em
info -- se
choice -- se
@enduml
```

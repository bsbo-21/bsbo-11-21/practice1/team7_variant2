```plantuml
@startuml
title Диаграмма последовательности Возврата книг(и)

Actor Библиотекарь as u
participant SearchEngine as se
participant Books_List as bl
participant LCard_List as cl
participant LibraryCard as lc
participant Book as b

u -> se ++: RequestCard(clientID)
se -> cl ++: GetLCard(clientID)
cl -> lc ++: Return Library Card
lc --> cl
deactivate lc
Return Return LibraryCard
se --> u : Return LibraryCard

loop для возвращённых книг
u -> se : TakeBookBack(book_to_return)

alt Если читательский билет был запрошен
  se -> bl ++: AddBook(book)
  bl -> b ++: Set Book as Not Taken
  deactivate b
  se -> cl ++: RemoveTakenBook(clientID, book)
  cl -> lc ++: RemoveDebt(book)
  deactivate lc
  deactivate cl
else Читательский билет не был проверен
  se -> se ++: throw Exception()
  deactivate se
  deactivate se
  deactivate bl
end
end

@enduml
```
